#define REVISION "X-2"
/* History:
*	X-0
*  25-feb-2013	njl display transfer rates as MB/sec rather than as
*		    kB/sec since few things seem that slow any longer.
*  18-Nov-2015 AHM Rename things from IOT to IOF.
*
*Revision History:
*		X-2		CRH		Connor Huggan			6-Jul-2020
*       Added conditional code to elapsed() to differentiate between 
*       how time is calculated on x86 vs ia64 and alpha. 
*		extern HWRPB *EXE$GPQ_HWRPB;
*		EXE$GPQ_HWRPB->hwrpb$iq_cycle_count_freq;
*		Added to get the cycle count frequency for SYS$RPCC_64()
*
*		X-1		CRH		Connor Huggan			10-Apr-2020
*		Initial check in
*/

#include "iof.h"
#include <math.h>
#include <hwrpbdef.h>
/*
 * Analysis of variance storage
 */

double		obs_sum[MAX_DEVICES];		/* Sum data in group	*/
double		obs_sumsq[MAX_DEVICES];		/* Sum of squared data	*/
double		obs_nsamp[MAX_DEVICES];		/* Number of samples	*/
double		total_obs;			/* Total observations	*/
double		total_sum;			/* Grand total data	*/
double		total_sumsq;			/* Grand sum of squares	*/
/*
 * ...df	Degrees of freedom	wg_... within group
 * ...sumsq	Sum of squares		bg_... between groups
 * ...meansq	Mean Square
 */
double		wg_df, wg_sumsq, wg_meansq;
double		bg_df, bg_sumsq, bg_meansq;
double		total_df;			/* Total degr. freedom	*/
double		F;				/* F ratio		*/
double		f_prob;				/* F significance	*/
//extern double	ftail();			/* Fisher's F-test	*/
extern unsigned __int64 itcMultiplier;

// Returns time difference in seconds, given two cycle-counter values;
//unsigned __int64 elapsed (unsigned __int64 start, unsigned __int64 end)
double elapsed (unsigned __int64 start, unsigned __int64 end)
{
//	unsigned __int64 difference;
	double cps;
	extern HWRPB *EXE$GPQ_HWRPB;
	double difference;
	double quotient;
//	unsigned __int64 quotient, remainder;
#ifdef __x86_64 /* X-2 */
	cps = (double)EXE$GPQ_HWRPB->hwrpb$iq_cycle_count_freq;
	float temp = ((float)((end - start) / cps));
	difference = (double)temp;
	return difference;
#else
	static int	divisor = 10000000.0;
	difference = (double) (((end - start) * itcMultiplier) >> 20);	// 100 ns
	quotient = difference / divisor;			// seconds
	return quotient;
#endif
}

void print_results (void)
{
	int			i;
	char			buffer[256];

	if (show & SHOW_CSV)
	    printf("device, q_depth, IOsize, count, duration, xfer_per_sec , Mb_per_sec\n");

	for (i = 0; i < iob_count; i++)
	    print_the_results(i, iobs[i]);

	if (iob_count > 1) {
	    compute_anova(iob_count);

	    if ((show & SHOW_CSV) == 0)
	    {
		if (f_prob < 0)
		    strcpy(buffer, "Total (p uncomputable)");
		else if (f_prob >= .025)
		    strcpy(buffer, "Total");
		else {
		    if (f_prob < .001)
			f_prob = .001;
		    sprintf(buffer, "Total (p < %0.3f)", f_prob);
		}

		print_heading(buffer,
		    elapsed(experiment_start, experiment_end),
		    (double) total_obs,
		    def.size * BYTES_PER_BLOCK
		    // def is of type PARAM, referenced in iof.h
		);
	    }

	    if ((show & SHOW_ANOVA) != 0)
		print_anova();
	}
}

void print_the_results (int group, $IOB * iob)
{
	int			i, j, k, low, high, factor;
	int			widest, mode, median, last;
	double			time, value, sum, sumsq, samples;
	double			mean, sd;
	double			halfway;	/* For median		*/
	double			bpt;		/* bytes per transfer	*/
	char			buffer[512];

	/*
	 * If we fail to assign a device, the IOB won't be filled in.
	 */
	if (S(IOB.devnam) == NULL)
	    return;

	sprintf (buffer, "Device, %s\nqueue depth, %d\ntransfer size (blocks), %d\n",
		S(IOB.devnam),
                IOB.cmd.queue_depth,
                IOB.cmd.size
	);

	print_heading(buffer,
	    elapsed(IOB.first_start, IOB.finished_time),
	    IOB.io_finished,
	    (IOB.cmd.size * BYTES_PER_BLOCK)
	);

	// Print the command parameters.
	printf ("operation, ");
	if (IOB.cmd.operation == Read) printf ("read");
	else if (IOB.cmd.operation == Write) printf ("write");
	else if (IOB.cmd.operation == Erase) printf ("erase");
	else if (IOB.cmd.operation == Check) printf ("check");
	else printf ("unknown");
	printf ("\n");

	printf ("pattern, ");
	if (IOB.cmd.selection == Random) printf ("random");
	else if (IOB.cmd.selection == Sequential) printf ("sequential");
	else if (IOB.cmd.selection == Maximum_seek) printf ("maximum seek");
	else printf ("unknown");
	printf ("\n");

	if (IOB.cmd.fastio) printf ("using $IO_PERFORM\n");
	else printf ("using $QIO\n");

	// Step through the histogram from the high end to the
	// low end looking for the first non-zero entry.
	// high is set to the first non-zero entry.
	// If all entries are zero, high will equal 0.
	for (high = HIST_SIZE; --high > 0;) {
	    if (IOB.histogram[high] != 0)
		break;
	}

	samples = 0.0;
	sum = 0.0;
	sumsq = 0.0;
	low = median = widest = -1;
	halfway = ((double) IOB.io_finished) / 2.0;
	for (i = 0; i <= high; i++) {
	    if (low < 0 && IOB.histogram[i] != 0)
		low = i;
	    if (IOB.histogram[i] > widest) {
		widest = IOB.histogram[i];
		mode = i;
	    }
	    time = MSEC(i);
	    value = (double) IOB.histogram[i];
	    samples += value;
	    sum += (time * value);
	    sumsq += (time * time * value);
	    // Note that this calculation of median should yield a value that
	    // represents all samples, not just those that fit into the histogram.
	    if (median < 0 && samples >= halfway)
		median = i;
	}
	obs_sum[group] = sum;
	obs_sumsq[group] = sumsq;
	obs_nsamp[group] = samples;
	total_obs += samples;
	total_sum += sum;
	total_sumsq += sumsq;
	if ((show & SHOW_HISTOGRAM) == 0)
	    return;
	if (samples == 0.0) {
	    printf("Bogus histogram: no values stored\n");
	    return;
	}
	if (IOB.out_of_range != 0)
	    printf("sample values out of range, %d\nmaximum value, %d\n", IOB.out_of_range, IOB.max_value);
	if (samples > 1.0) {
	    mean = sum / samples;
	    sd = sqrt((sumsq - ((sum * sum) / samples)) / (samples - 1.0));
	    printf("minimum, %d\nmax histogram sample, %d\nmedian, %d\nmode, %d\n",
		    MSEC(low), MSEC(high), MSEC(median), MSEC(mode));
	    printf("mean, %0.1f\nstandard deviation, %0.1f\n", mean, sd);

	    iof_set_symbol_str ("IOF$DEVICE", S(IOB.devnam));
	    iof_set_symbol_int ("IOF$MIN", MSEC(low));
	    iof_set_symbol_int ("IOF$MAX", MSEC(high));
	    iof_set_symbol_int ("IOF$MEDIAN", MSEC(median));
	    iof_set_symbol_int ("IOF$MODE", MSEC(mode));
	    iof_set_symbol_double ("IOF$MEAN", mean);
	    iof_set_symbol_double ("IOF$STANDARD_DEVIATION", sd);
	}
	
	for (factor = 1; (widest / factor) > 50; factor++)
	    ;
	last = mean + (sd * 2.0);	// only print histogram out to 2 SD above mean
	if (last > high)
	    last = high;
	// Print out the entire histogram range except the zero tails.
	printf("\nHistogram of transfer times (microseconds)\n");
//	for (i = 0; i <= last; i++) {
	for (i = 0; i <= high; i++) {
	    if (IOB.histogram[i] == 0) {
		for (k = i; k < high; k++) {
		    if (IOB.histogram[k] != 0)
			break;
		}
	    }	    
	    printf("%4d:%10d ", MSEC(i), IOB.histogram[i]);
	    for (j = (IOB.histogram[i] + (factor - 1)) / factor; --j >= 0;)
		putchar('X');
	    putchar('\n');
	}
//	if (last < high) {
//	    int		ignored;
//	    int		slowest;
//
//	    for (ignored = 0, slowest = -1, i = last + 1; i <= high; i++) {
//		ignored += IOB.histogram[i];
//		if (IOB.histogram[i] != 0)
//		    slowest = i;
//	    }
//	    if (ignored != 0) {
//		printf("%d samples not printed, slowest at %d usec\n",
//		    ignored, MSEC(slowest));
//	    }
//	}
}

// Assumes duration is in seconds.
void print_heading (char * name, double duration, double transfers, int size)
{
//	double durationdbl = (double) duration / 100.0;		// convert duration to tenths of a second

	if ((show & SHOW_SUMMARY) != 0) {
	    printf("%22snumber of transfers, %0.0f\nduration (sec), %0.3f\n", name, transfers, duration);
	    if (duration > 0.0) {
		    printf("transfers/sec, %0.1f\nMB/sec, %0.2f",
			transfers / duration,
			(transfers * (double) size) / (1024.0 * 1024.0 * duration));

//	    if ((show & SHOW_CSV) == 0)
//		printf(" (q_depth size #xfer duration(sec) xfer/sec Mb/sec)");

	    if (appendstring)
		printf(", %s", appendstring);
	    }
	    printf("\n");
	}
}

void compute_anova (int ngroups)
/*
 * print_histogram was kind enough to collect the anova values.
 * We just print the analysis itself.
 */
{
	int		i;
	double		mean_tot_sq;

	if (total_obs == 0)
	    goto nogood;
	mean_tot_sq = (total_sum * total_sum) / total_obs;
	total_sumsq -= mean_tot_sq;
	/*
	 * Get sum of squares for between- and within-groups.
	 */
	bg_sumsq = 0.0;
	for (i = 0; i < ngroups; i++) {
	    if (obs_nsamp[i] == 0)
		goto nogood;
	    bg_sumsq += ((obs_sum[i] * obs_sum[i]) / obs_nsamp[i]);
	}
	bg_sumsq -= mean_tot_sq;
	wg_sumsq = total_sumsq - bg_sumsq;
	/*
	 * Get degrees of freedom
	 */
	total_df = total_obs - 1;
	wg_df    = total_obs - ngroups;
	bg_df    = ngroups - 1;
	if (bg_df == 0 || wg_df == 0)
	    goto nogood;
	/*
	 * Mean squares and F-ratio
	 */
	bg_meansq = bg_sumsq / bg_df;
	wg_meansq = wg_sumsq / wg_df;
	if (wg_meansq == 0.0)
	    goto nogood;
	F = bg_meansq / wg_meansq;
	if (bg_df >= 1.0
	 && wg_df >= 1.0)
	    f_prob = ftail(F, bg_df, wg_df);
	else {
nogood:	    f_prob = -1.0;
	}
}

void print_anova (void)
{
	printf("Analysis of Variance\n");
	printf("Source of Variation Sum of Squares  d.f.  Mean Square");
	printf("    F    sig.\n");
	printf("Between Groups %19.1f %5.0f %12.2f %6.2f %6.3f\n",
	    bg_sumsq, bg_df, bg_meansq, F, f_prob);
	printf("Within Groups  %19.1f %5.0f %12.2f\n",
	    wg_sumsq, wg_df, wg_meansq);
	printf("Total          %19.1f %5.0f\n",
	    total_sumsq, total_df);
	if (f_prob >= 0.0 && f_prob < 0.001)
	    f_prob = 0.001;
	if (f_prob > 0.05)
	    printf("Variance not significant (p = %.03f)\n", f_prob);
	else if (f_prob >= 0.0) {
	    printf("Variance significant < %0.3f\n", f_prob);
	}
	else {
	    printf("Cannot calculate significance\n");
	}
}
